# -*- coding: utf-8 -*-

import sys
sys.path.append('../trade_up/')

import json
import time
import pickle
import logging
import yaml
from ast import literal_eval
import pandas as pd
import numpy as np
from meinheld import server
from flask import Flask, request, jsonify
from Fast_set_matching_clean import generate_index
from model_rec import AR_recommend, global_rec, ab_test, get_item_code, XGB_recommend, reboot_redis_inner
import traceback


app = Flask(__name__)

conf_path = "../config/config.yaml"

with open(conf_path, encoding="utf-8") as f:
    config = yaml.safe_load(f)

# start logger
# formate = json.dumps({"time": "%(asctime)s", "levelname":"%(levelname)s", "message":"%(message)s"})
# logging.basicConfig(filename=config["logging_file"], filemode="a", level=logging.DEBUG,
                    # format=formate)
    
logger = logging.getLogger('api')
logger.setLevel(logging.DEBUG)

main_fh = logging.FileHandler('/opt/log/server/msRecWeb/api.log')#config["logging_file"])
main_fh.setLevel(level = logging.DEBUG)
main_fh.setFormatter(logging.Formatter(json.dumps({"time": "%(asctime)s", "levelname":"%(levelname)s", "message":"%(message)s"})))
logger.addHandler(main_fh)

# initialization
start_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
logger.info("ms_server_start_time: " + start_time)

@app.errorhandler(Exception)
def error_handler(e):
    """全局异常捕获"""
    data = {
        "retCode": -1,
        "message": str(e),
        "data": []
    }
    logger.error("Error request is: " + str(request.get_data()))
    logger.error("ms_server_error: " +  str(e))
    logger.error("Trace back error: " +  traceback.format_exc())
    return jsonify(data)

@app.route("/test/model_matrix", methods=["POST"])
def testing_model_model_matrix():
    if request.method == "POST":
        json_data = request.get_data()
        json_data = json.loads(json_data)
        uniqueId = json_data["uniqueId"]
        recom_result = XGB_recommend(json_data, retVal = 'model_matrix')
        data = {"uniqueId": uniqueId, "list": recom_result}
        return jsonify(retCode=200, message="success", data=data)

@app.route("/test/model_predict", methods=["POST"])
def testing_model_predict():
    if request.method == "POST":
        json_data = request.get_data()
        json_data = json.loads(json_data)
        uniqueId = json_data["uniqueId"]
        recom_result = XGB_recommend(json_data, retVal = 'model_predict')
        data = {"uniqueId": uniqueId, "list": recom_result}
        return jsonify(retCode=200, message="success", data=data)
    
@app.route("/rec", methods=["POST"])
def tradeup_server():
    """web server"""
    if request.method == "POST":
        json_data = request.get_data()
        json_data = json.loads(json_data)
        if not isinstance(json_data, dict):
            logger.error("invalid request format")
            return jsonify(retCode=-1, message="invalid request format", data=[])
        uniqueId = json_data["uniqueId"]
        if uniqueId == "":
            logger.error("uniqueId cannot be None")
            return jsonify(retCode=-1, message="uniqueId cannot be null", data=[])
        userCode = json_data["userCode"]
        transactionId = json_data["transactionId"]
        # cityCode = json_data["cityCode"]
        # storeCode = json_data["storeCode"]
        crowd = ab_test(userCode)
        if crowd == 'xgb':
            recom_result = XGB_recommend(json_data)
            ab_crowd = uniqueId + f"_" + transactionId + f"_" + userCode + f"_" + str(len(recom_result)) + f"_" + str(2)
            logger.info(ab_crowd)
            #logging.info('through XGBoost')
            data = {"uniqueId": uniqueId, "list": recom_result}
        
        elif crowd == 'fpg':
            shoppingCart = json_data["shoppingCart"]
            shopping_cart = get_item_code(shoppingCart)
            recom_result = AR_recommend(shopping_cart)
            ab_crowd = uniqueId + f"_" + transactionId + f"_" + userCode + f"_" + str(len(recom_result)) + f"_" + str(1)
            logger.info(ab_crowd)
            data = {"uniqueId": uniqueId, "list": recom_result}
                
        elif crowd == 'control':
            data = {"uniqueId": uniqueId, "list": global_rec}
            ab_crowd = uniqueId + f"_" + transactionId + f"_" + userCode + f"_" + str(7) + f"_" + str(0)
            logger.info(ab_crowd)
        else:
            pass
        
        return jsonify(retCode=200, message="success", data=data)
    return "hello world"


@app.get('/reboot_redis')
def reboot_redis_connector():
    return_code = reboot_redis_inner()
    if return_code:
        return jsonify(retCode=200, message="redis connector reboot success")
    else:
        return jsonify(retCode=500, message="redis connector reboot failed")





if __name__ == "__main__":

    # server
    server.listen(("0.0.0.0", 8080))
    server.run(app)









