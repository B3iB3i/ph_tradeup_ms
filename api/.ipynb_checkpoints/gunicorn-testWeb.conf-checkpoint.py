# gunicorn.conf.py
# Non logging stuff
bind = "0.0.0.0:8080"
workers = 8
# Access log - records incoming HTTP requests
accesslog = "/opt/log/server/msRecWeb/gunicorn.access.log"
# Error log - records Gunicorn server goings-on
errorlog = "/opt/log/server/msRecWeb/gunicorn.error.log"
# Whether to send Django output to the error log
capture_output = True
# How verbose the Gunicorn error logs should be
loglevel = "info"
#certfile = 'cert.pem'   # ssl证书文件
#keyfile = 'key.pem'   # ssl key文件