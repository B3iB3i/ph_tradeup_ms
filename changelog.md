commit: zhenyu weinan
final modification timestamp 11/30 20:50
changelog:
- updated resources file
- fixed bugs in combine_feature
- changed combine_feature output, model predict input parameters
** added .astype(np.float64) might affect precision a little bit (less than 1e-12)
- changed model_rec.XGB_recommend. 


=============================================
** test version !!! 
commit: weinan
final modification timestamp: 11/29 22:00
changelog:
- testing / validaton purpose only

====================================
commit: weinan
final modification timestamp: 11/25 17:17
changelog:
- adjusted logger. make sure redis / main api goes to different logging file

==================================================
commit: weinan
final modification timestamp: 11/25 16:00
changelog:
- different logs for redis
- new model with oversampling
