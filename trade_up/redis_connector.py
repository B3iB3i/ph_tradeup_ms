import redis
import logging
import json

class TradeUpConnectionPool:
    def __init__(self, logging_path, max_connections = 10):
        # start logger
        self.logging_path = logging_path
        self.logger = self.logger_init(logging_path)
        
        self.hosts = ['172.24.57.16', '172.24.57.17']
        self.connection_configs = {'port' : 6379,
                                    'password' : 'Meritco2022',
                                    'decode_responses' : True}
        self.max_connections = max_connections
        
        self.pools = [redis.connection.ConnectionPool(**self.connection_configs, max_connections = self.max_connections, host = self.hosts[i]) for i in [0,1]]
        self.pool = self.pools[0]
        self.mode = 0 # 0 - main, 1 - slave, 2 - planB
        self.planB_return = {'Pizza_percent': '0.0',
                             'Protein_percent': '0.0',
                             'average_transaction_amount': '0.0',
                             'Others_percent': '0.0',
                             'Breakfast_MainCourse_percent': '0.0',
                             'Pasta_percent': '0.0',
                             'Appetizer_percent': '0.0',
                             'Dessert_percent': '0.0',
                             'Soup_percent': '0.0',
                             'Rice_percent': '0.0',
                             'Drink_percent': '0.0',
                             'order_num': '0',
                             'Salad_percent': '0.0'}
        
    def logger_init(self, logging_path):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        
        fh = logging.FileHandler(logging_path)
        fh.setLevel(level = logging.DEBUG)
        fh.setFormatter(logging.Formatter(json.dumps({"time": "%(asctime)s", "levelname":"%(levelname)s", "message":"%(message)s"})))
        logger.addHandler(fh)
        
        logger.critical('redis connector logger init succeed.')
        
        return logger
    
    def switch_node(self): # master>slave>backup
        self.mode += 1
        
        if self.mode not in [0, 1]:
            self.logger.critical('Critical error. Both master and slave are down. Returning default values (all 0s)')
            
        elif self.mode == 1:
            self.pool = self.pools[1]
            self.logger.critical(f'Critical error. Master down. Connecting to slave. Current host = {self.hosts[self.mode]}')
            
        else:
            self.logger.critical(f'Critical error. Mode = 0 after switching. This should never happen.')
            raise Exception('unknown mode')
            
    def reboot(self): # master>slave>backup
        try:
            self.__init__(logging_path=self.logging_path, max_connections = self.max_connections)
            self.logger.critical(f'Redis connector rebooted, current host = {self.hosts[self.mode]}')
            return True
        except:
            return False

    
    def get(self, key): # auto switch connection
        if self.mode in [0,1]:
            r = redis.Redis(**self.connection_configs, host = self.hosts[self.mode], connection_pool = self.pool)
            return_value = r.hgetall(key)
            if return_value == {}:
                self.logger.warning(f'Redis returned empty dict. Returning default user feature for {key}')
                return_value=self.planB_return
            return return_value
        else:
            self.logger.warning(f'Servers down. Returning default user feature for {key}')
            return self.planB_return
    
    def get_user_feature(self, user_code, max_tries = 5):
        key = "u:" + user_code
        for n_tries in range(max_tries):
            try:
                #logging.critical(f'trying hgetall {key}')
                return self.get(key)
            except:
                self.logger.warning(f'get_user_feature failed, trying {n_tries}th time, user code = {user_code}')
        self.switch_node()
        return self.get_user_feature(user_code)


