#%% Fabriate data
import random

def generate_index(k, MAX_NODE = 500000):
    #a directed tree with only the root node. format of the node (nodeid(key):[list of children])
    node_status = [0]*MAX_NODE #0 for unused, 1 for node on a path, 2 for end node
    node_name = [-1]*MAX_NODE
    node_path = [[] for i in range(MAX_NODE)]
    child_list_pointer = [[] for i in range(MAX_NODE)]
    child_list_name = [[] for i in range(MAX_NODE)]
    
    node_status[0] = 1 #initialize root node
    node_name[0] = 0
    node_path[0] = tuple()
    next_unused_node = 1
    
    for s in k: # for each anticedents
        s = sorted(s)
        p = 0 #start from root node
        for e_name in s: # for elements in antecedent
            if e_name not in child_list_name[p]:
                child_list_name[p].append(e_name)
                child_list_pointer[p].append(next_unused_node)
                node_status[next_unused_node] = 1
                node_name[next_unused_node] = e_name
                node_path[next_unused_node] = node_path[p]+(e_name,)
                next_unused_node += 1
            p = child_list_pointer[p][child_list_name[p].index(e_name)]
        node_status[p] = 2
    
    #To avoid using list index method in fast_match    
    name_to_pointer = [dict(zip(i,j)) for i,j in zip(child_list_name, child_list_pointer)]
    
    return node_status[:next_unused_node], \
           node_name[:next_unused_node], \
           node_path[:next_unused_node], \
           child_list_name[:next_unused_node], \
           child_list_pointer[:next_unused_node], \
           name_to_pointer[:next_unused_node]

    
#%% fast set match algo
#from numba import jit #numba doesn't work well. 50% slower than pure python code
        
def remove_smaller_sets(r):
    r = sorted(r,key=lambda x:len(x),reverse=True)
    r = [frozenset(i) for i in r]
    ind = 0
    while ind < len(r)-1:
        r = r[:ind+1] + [j for j in r[ind+1:] if not j.issubset(r[ind])]
        ind += 1
    r= [tuple(sorted(list(i))) for i in r]
    return r


MAX_POINTER_NUM = 1000
def fast_match(s, name_to_pointer, node_status, node_path): #s is any sorted iterables
    s = sorted(s) #force sorting
    
    pts=[0]*MAX_POINTER_NUM
    pts_superceded = [0]*MAX_POINTER_NUM #perform simple partial removal of subsets
    pts_num = 1
    
    for e in s:
        #print(e)
        for ind in range(pts_num) :
            p = pts[ind]
            #move down, and current pointer is superceded by next pointer
            if e in name_to_pointer[p]:#e in child_list_name[p]: 10x faster!
                pts_superceded[ind] = 1 #perform simple partial removal of subsets
                next_p = name_to_pointer[p][e] #major acceleration than #next_p = child_list_pointer[p][child_list_name[p].index(e)]
                pts[pts_num] = next_p
                pts_num+=1
#        a = pts[:pts_num]
#        b = pts_superceded[:pts_num]
#        print(['%d-%d'%(ss[0],ss[1]) for ss in zip(a,b)])
    #print("max pointers collected:",pts_num)
    
    results = [None]*MAX_POINTER_NUM
    num_of_results = 0
    for ind in range(pts_num):
        p = pts[ind]
        if node_status[p]==2: # and pts_superceded[ind]==0: BUG: partial removal is incorrect when a node superceded its parent is only a status 1 (path node)
            results[num_of_results]=node_path[p]
            num_of_results+=1
    return remove_smaller_sets(results[:num_of_results])


