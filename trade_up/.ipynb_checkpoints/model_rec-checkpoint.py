# -*- coding: utf-8 -*-

import json
import time
import yaml
import pickle
import hashlib
from ast import literal_eval
import pandas as pd
import numpy as np
from Fast_set_matching_clean import fast_match, generate_index
import pandas as pd
from datetime import datetime
from xgboost import XGBClassifier
import daal4py as d4p
from redis_connector import TradeUpConnectionPool


def load_dict(path):
    """load dict from json file"""
    with open(path, "rb") as dict_file:
        dic = pickle.load(dict_file)
    return dic


def get_ab_mapping(ab_grouping):
    total = sum(ab_grouping.values())
    accum = 0
    ab_split = {}
    for k,v in ab_grouping.items():
        accum += float(v)
        ab_split[accum/total] = k
    ab_mapping = {}
    for idx, hashed_code in enumerate(bins):
        percentage = idx/len(bins)
        ab_mapping[hashed_code] = next(key for split, key in ab_split.items() if split > percentage)
    return ab_mapping


def hash_id(user_code):
    "hash user_code"
    md = hashlib.md5()
    md.update(user_code.encode('utf-8'))
    md_v = md.hexdigest()
    return md_v


def ab_test(user_code):
    md_v = hash_id(user_code)
    return ab_mapping[md_v[8:10]]

    
def get_item_code(shopping_cart):
    """"return sku code"""
    shopping_item_lst = set()
    for i in shopping_cart:
        if i["type"] == '1':
            for j in i["items"]:
                shopping_item_lst.add(j["linkId"])
        else:
            shopping_item_lst.add(i["linkId"])
    return shopping_item_lst


def pre_process(shopping_cart):
    """return itemcode category"""
    itemcode_cart = set([linkid2itemcode_dict.get(i) for i in shopping_cart if linkid2itemcode_dict.get(i) is not None])
    item_category_cart = set([category_dict.get(i) for i in itemcode_cart if category_dict.get(i) is not None])
    return item_category_cart, itemcode_cart


def fpg_recom(itemcode_cart):
    """association rule matching"""
    match_key = fast_match(itemcode_cart, name_to_pointer, node_status, node_path)
    #print('match_key', match_key)
    pre_recom_list = []
    for i in match_key:
        i = tuple(sorted(i))
        if association_rule_dict.get(i) is not None:
            pre_recom_list += association_rule_dict.get(i)
    # print(pre_recom_list)
    return pre_recom_list


def recom_path_1(pre_recom_list, itemcode_cart):
    """no recommend category"""
    recom_dict = {}
    promotioncode_cart = set()
    pre_recom_list = pre_recom_list + hot_list[:40]
    pre_recom_list = sorted(pre_recom_list, key=lambda x:list(x.values()), reverse=True)
    for i in itemcode_cart:
        if itemcode2promotioncode_dict.get(i) is not None:
            for j in itemcode2promotioncode_dict.get(i):
                promotioncode_cart.add(j)
    for i in pre_recom_list:
        for k, v in i.items():
            for j in k:
                p_code = itemcode2promotioncode_dict.get(j)[0]
                if p_code is None:
                    continue
                else:
                    if p_code not in promotioncode_cart:
                        item_price = max(float(price_dict.get(j, 1)), 1)
                        if p_code not in recom_dict:
                            recom_dict[p_code] = v*item_price
                        else:
                            recom_dict[p_code] = max(v*item_price, recom_dict[p_code])
        if len(recom_dict) == 12:
            break
    return recom_dict


def recom_path_2(pre_recom_list, item_category_cart):
    """recommend basede on category"""
    recom_dict = {}
    pre_recom_list = pre_recom_list + hot_list[:40]
    pre_recom_list = sorted(pre_recom_list, key=lambda x:list(x.values()), reverse=True)
    for i in pre_recom_list:
        for k, v in i.items():
            for j in k:
                p_category = category_dict.get(j)
                p_code = itemcode2promotioncode_dict.get(j)[0]
                if p_code is None:
                    continue
                else:
                    if p_category not in item_category_cart:
                        item_price = max(float(price_dict.get(j, 1)), 1)
                        if p_code not in recom_dict:
                            recom_dict[p_code] = v*item_price
                        else:
                            recom_dict[p_code] = max(v*item_price, recom_dict[p_code])
        if len(recom_dict) == 12:
            break
    return recom_dict


def post_process(pre_recom_list, item_category_cart, itemcode_cart):
    """post processes based on rules"""
    rank = 1
    recom_result = []
    if len(item_category_cart & recom_category) == 5:
        recom_dict = recom_path_1(pre_recom_list, itemcode_cart)
    else:
        recom_dict = recom_path_2(pre_recom_list, item_category_cart)

    recom_lst = sorted(recom_dict.items(), key=lambda x: x[1], reverse=True)
    recom_lst = [i[0] for i in recom_lst]
    #recom_score = [str(round(i[1],4)) for i in recom_result]
    for i in recom_lst:
        r_item = {"promotionCode": i, "rank": rank}
        recom_result.append(r_item)
        rank += 1
    return recom_result[:12]


def rec_for_error(hot_list):
    """return global recommend items if error"""
    rank = 1
    recom_dict = {}
    recom_result = []
    pre_recom_list = hot_list
    for i in pre_recom_list:
        for k, v in i.items():
            p_code = itemcode2promotioncode_dict.get(k[0])[0]
            if p_code is None:
                continue
            else:
                item_price = max(float(price_dict.get(k[0], 1)), 1)
                if p_code not in recom_dict:
                    recom_dict[p_code] = v * item_price
                else:
                    recom_dict[p_code] = max(v * item_price, recom_dict[p_code])
        if len(recom_dict) == 12:
                break
    recom_lst = sorted(recom_dict.items(), key=lambda x: x[1], reverse=True)
    recom_lst = [i[0] for i in recom_lst]
    for i in recom_lst:
        r_item = {"promotionCode": i, "rank": rank}
        recom_result.append(r_item)
        rank += 1
    return recom_result


def AR_recommend(shopping_cart: list):
    """return recommend items with promotion code"""
    
    item_category_cart, itemcode_cart = pre_process(shopping_cart)
    pre_recom_list = fpg_recom(itemcode_cart)
    recom_result = post_process(pre_recom_list, item_category_cart, itemcode_cart)
    return recom_result


def get_shoppingcart_info_lst(shopping_cart):
    """"return sku code"""
    shopping_item_lst = list()
    num_lst = list()
    type_lst = list()
    for i in shopping_cart:
        if i["type"] == '1':
            for j in i["items"]:
                shopping_item_lst.append(j["linkId"])
                num_lst.append(j["num"])
                
        else:
            shopping_item_lst.append(i["linkId"])
            num_lst.append(i["num"])
        type_lst.append(i["type"])
    return shopping_item_lst, num_lst, type_lst


def generate_online_feature(input_json):
    shopping_cart_info = input_json['shoppingCart']
    linkid_list, num_list, type_lst = get_shoppingcart_info_lst(shopping_cart_info)
    order_time = int(input_json['orderTime'])
    day_of_week = datetime.fromtimestamp(order_time).weekday()
    
    contains_combo = 0
    if '1' in type_lst:
        contains_combo = 1

    item_code_list = [linkid2itemcode_dict.get(i) for i in linkid_list]
    spu_list = [item_code_spu_dict.get(i, "Others") for i in item_code_list]

    online_feature_dict = {"total_price": 0, "total_sub_category_num":0, "Appetizer_num":0, "Drink_num":0, "Pizza_num":0, 
                    "Pasta_num":0, "Dessert_num":0, "Rice_num":0, 'Others_num':0,
                    "Salad_num":0, 'Protein_num':0, "Breakfast_MainCourse_num":0, "Soup_num":0}
    
    target_spu = ["Appetizer", "Drink", "Pizza",
                    "Pasta", "Dessert", "Rice", 'Others',
                    "Salad", 'Protein', "Breakfast_MainCourse", "Soup"]

    cleaned_spu_list = [i if i in target_spu else "Others" for i in spu_list]

    for i,j in zip(cleaned_spu_list, num_list):
        if j == "":
            continue
        else:
            online_feature_dict[f"{i}_num"] += int(j)
    
    online_feature_dict['total_price'] = float(input_json['realOrderAmount'])/100.00
    online_feature_dict['total_sub_category_num'] = len(set(cleaned_spu_list))
    online_feature_dict['contains_combo'] = contains_combo
    online_feature_dict['day_of_week'] = day_of_week
    online_feature_dict['daypart_name'] = int(input_json['daypart'])
    
    feature_lst = []
    
    for i in online_feature_name_lst:
        feature_lst.append(online_feature_dict[i])

    feature_array = np.array(feature_lst)
    
    return feature_array


def generate_user_feature(user_code):
    user_feature_dict = conn.get_user_feature(user_code)
    user_feature_arr = np.array([float(user_feature_dict.get(i, 0)) for i in user_feature_name_list])
    return user_feature_arr


def generate_fpg_output(shopping_cart: list):
    """return recommend items with promotion code"""
    
    item_category_cart, itemcode_cart = pre_process(shopping_cart)
    pre_recom_list = fpg_recom(itemcode_cart)
#     fpg_output = fill_fpg_itemcode(pre_recom_list, item_category_cart, itemcode_cart)
#     fpg_output_lst = sorted(fpg_output.items(), key=lambda x: x[1], reverse=True)

#     fpg_output_dict = {}

#     rank = 1
#     for i in fpg_output_lst:
#         fpg_output_dict[i[0]] = [i[1], rank]
#         rank += 1
    fpg_output_dict = post_process_with_c(pre_recom_list, item_category_cart, itemcode_cart)

    return fpg_output_dict


def recom_path_1_xgb(pre_recom_list, itemcode_cart):
    """no recommend category"""
    recom_dict = {}
    promotioncode_cart = set()
    pre_recom_list = pre_recom_list + hot_list
    pre_recom_list = sorted(pre_recom_list, key=lambda x:list(x.values()), reverse=True)
    for i in itemcode_cart:
        if itemcode2promotioncode_dict.get(i) is not None:
            for j in itemcode2promotioncode_dict.get(i):
                promotioncode_cart.add(j)
    for i in pre_recom_list:
        for k, v in i.items():
            for j in k:
                p_code = itemcode2promotioncode_dict.get(j)[0]
                if p_code is None:
                    continue
                else:
                    if p_code not in promotioncode_cart:
                        if j not in recom_dict:
                            recom_dict[j] = v
                        else:
                            recom_dict[j] = max(v, recom_dict[j])
        if len(recom_dict) == xgb_recom_num:
            break
    return recom_dict


def recom_path_2_xgb(pre_recom_list, item_category_cart):
    """recommend basede on category"""
    recom_dict = {}
    pre_recom_list = pre_recom_list + hot_list
    pre_recom_list = sorted(pre_recom_list, key=lambda x:list(x.values()), reverse=True)
    for i in pre_recom_list:
        for k, v in i.items():
            for j in k:
                p_category = category_dict.get(j)
                p_code = itemcode2promotioncode_dict.get(j)[0]
                if p_code is None:
                    continue
                else:
                    if p_category not in item_category_cart:
                        if j not in recom_dict:
                            recom_dict[j] = v
                        else:
                            recom_dict[j] = max(v, recom_dict[j])
        if len(recom_dict) == xgb_recom_num:
            break
    return recom_dict


def fill_fpg_itemcode(pre_recom_list, item_category_cart, itemcode_cart):
    """post processes based on rules"""
    # rank = 1
    # recom_result = []
    # if len(item_category_cart & recom_category) == 5:
    recom_dict = recom_path_1_xgb(pre_recom_list, itemcode_cart)
    # else:
    #     recom_dict = recom_path_2_xgb(pre_recom_list, item_category_cart)

    return recom_dict


def recom_path_1_with_c(pre_recom_list, itemcode_cart):
    """no recommend category"""
    recom_dict = {}
    promotioncode_cart = set()
    pre_recom_list = pre_recom_list + hot_list[:40]
    pre_recom_list = sorted(pre_recom_list, key=lambda x:list(x.values()), reverse=True)
    for i in itemcode_cart:
        if itemcode2promotioncode_dict.get(i) is not None:
            for j in itemcode2promotioncode_dict.get(i):
                promotioncode_cart.add(j)
    for i in pre_recom_list:
        for k, v in i.items():
            for j in k:
                p_code = itemcode2promotioncode_dict.get(j)[0]
                if p_code is None:
                    continue
                else:
                    if p_code not in promotioncode_cart:
                        item_price = max(float(price_dict.get(j, 1)), 1)
                        if p_code not in recom_dict:
                            recom_dict[p_code] = (v*item_price, v)
                        else:
                            recom_dict[p_code] = (max(v*item_price, recom_dict[p_code][0]), max(v, recom_dict[p_code][1]))
        if len(recom_dict) == 12:
            break
    return recom_dict


def recom_path_2_with_c(pre_recom_list, item_category_cart):
    """recommend basede on category"""
    recom_dict = {}
    pre_recom_list = pre_recom_list + hot_list[:40]
    pre_recom_list = sorted(pre_recom_list, key=lambda x:list(x.values()), reverse=True)
    for i in pre_recom_list:
        for k, v in i.items():
            for j in k:
                p_category = category_dict.get(j)
                p_code = itemcode2promotioncode_dict.get(j)[0]
                if p_code is None:
                    continue
                else:
                    if p_category not in item_category_cart:
                        item_price = max(float(price_dict.get(j, 1)), 1)
                        if p_code not in recom_dict:
                            recom_dict[p_code] = (v*item_price, v)
                        else:
                            recom_dict[p_code] = (max(v*item_price, recom_dict[p_code][0]), max(v, recom_dict[p_code][1]))
        if len(recom_dict) == 12:
            break
    return recom_dict


def post_process_with_c(pre_recom_list, item_category_cart, itemcode_cart):
    """post processes based on rules"""
    rank = 1
    recom_result = []
    if len(item_category_cart & recom_category) == 5:
        recom_dict = recom_path_1_with_c(pre_recom_list, itemcode_cart)
    else:
        recom_dict = recom_path_2_with_c(pre_recom_list, item_category_cart)

    recom_lst = sorted(recom_dict.items(), key=lambda x: x[1][1], reverse=True)
    recom_item_lst = [i[0] for i in recom_lst]
    recom_conf_lst = [i[1][1] for i in recom_lst]
    #recom_score = [str(round(i[1],4)) for i in recom_result]
    for i in zip(recom_item_lst, recom_conf_lst):
        r_item = {"promotionCode": i[0], "rank": rank, "confidence": i[1]}
        recom_result.append(r_item)
        rank += 1
        
    recom_result = recom_result[:12]
    # print(recom_result)
    fpg_output_dict = {}
    for i in recom_result:
        fpg_output_dict[promotioncode2itemcode_dict.get(i["promotionCode"])] = [i["confidence"], i["rank"]]

    return fpg_output_dict


# ----- preload
def load_model(model_path):
    xgbc = XGBClassifier()
    xgbc.load_model(model_path)
    return xgbc

# ----- combine features

def combine_features(user_features,
                    item_feature_dict: dict, 
                    shoppingcart_features,
                    fpg_output
                    ):
    one_row = np.concatenate([shoppingcart_features, user_features])
    model_matrix = np.concatenate([np.broadcast_to(one_row, (len(fpg_output), one_row.shape[0])), 
                            np.stack([v + item_feature_dict.get(k, impute_item_features) + [k] for k, v in fpg_output.items()], axis = 0)], axis = 1)
    return model_matrix


def model_predict(model_matrix):
    item_code_list = list(model_matrix[:,-1])
    pred = d4p.gbt_classification_prediction(nClasses=2, resultsToEvaluate='computeClassProbabilities').compute(model_matrix[:,:-1].astype(np.float64), daal_model).probabilities[:,1]
    result_dict = dict(zip(item_code_list, pred))
    return result_dict


def xgb_model_result_post_process(model_result):
    promotioncode_sore_dict = {}

    for k,v in model_result.items():
        item_price = max(float(price_dict.get(k, 1)), 1)
        promotion_code = itemcode2promotioncode_dict.get(k)[0]

        if promotion_code is None:
            continue
        else:
            promotioncode_sore_dict[promotion_code] = v*item_price
    
    rank = 1
    recom_result = []

    recom_lst = sorted(promotioncode_sore_dict.items(), key=lambda x: x[1], reverse=True)
    recom_lst = [i[0] for i in recom_lst]
    #recom_score = [str(round(i[1],4)) for i in recom_result]
    for i in recom_lst:
        r_item = {"promotionCode": i, "rank": rank}
        recom_result.append(r_item)
        rank += 1
    
    return recom_result[:xgb_recom_num]


def XGB_recommend(input_json, retVal = None):
    shopping_cart_lst = input_json['shoppingCart']
    fpg_output = generate_fpg_output(get_item_code(shopping_cart_lst))
    
    online_feature = generate_online_feature(input_json)
    user_feature = generate_user_feature(input_json['userCode'])
    
    model_matrix = combine_features(user_feature, item_feature_dict, online_feature, fpg_output)
    # passed item_featurs, should pass item_feature_dict. resulting in default return all the time
    model_result = model_predict(model_matrix)
    
    recom_result = xgb_model_result_post_process(model_result)
    
    if retVal is None:
        return recom_result
    
    elif retVal == 'model_matrix':
        return model_matrix.tolist()
    
    elif retVal == 'model_predict':
        return model_result#


def reboot_redis_inner():
    return conn.reboot()



conf_path = "../config/config.yaml"

with open(conf_path, encoding="utf-8") as f:
    config = yaml.safe_load(f)


# load resources
linkid2itemcode_dict = load_dict(config["linkid_to_itemcode_dict"])
association_rule_dict = load_dict(config["fpg_dict"])
itemcode2promotioncode_dict = load_dict(config["itemcode_to_promotioncode_dict"])
price_dict = load_dict(config["price_dict"])
antecedant_df = pd.read_csv(config["antecedant_df"])
hot_dict = load_dict(config["hot_item_dict"])
category_dict = load_dict(config["category_dict"])

promotioncode2itemcode_dict = {}
for k, v in itemcode2promotioncode_dict.items():
    promotioncode2itemcode_dict[v[0]] = k

# generate matching index
antecedant_lst = antecedant_df["antecedants"].map(literal_eval).to_list()
antecedant_lst = [tuple(sorted(i)) for i in antecedant_lst]
node_status, node_name, node_path, child_list_name, child_list_pointer, name_to_pointer = generate_index(antecedant_lst)

hot_list = hot_dict["hot_items"]
recom_category=set(config['recom_category'])

# 全局兜底
#global_rec = rec_for_error(hot_list)
global_rec = [{"promotionCode": "04O4LMYPW6", "rank": 1},{"promotionCode": "4TNMF6SEG0", "rank": 2},{"promotionCode": "4POKGJLS3O", "rank": 3},{"promotionCode": "EAPLXC8JAP", "rank": 4},{"promotionCode": "YRHG4W79V9", "rank": 5},{"promotionCode": "ED5YBREITD", "rank": 6},{"promotionCode": "6N62TJUN9A", "rank": 7}]

# ab test
ab_grouping = config['ab_test']['ab_grouping']#{'control':0.1, 'fpg':0.8, 'xgb':0.1}
bins = [f"{i}{j}" for i in '0123456789abcdef' for j in '0123456789abcdef']

ab_mapping = get_ab_mapping(ab_grouping)

# redis connector
conn = TradeUpConnectionPool(logging_path = config["redis"]["logging_file"], max_connections = int(config['redis']['max_connections']))

#xgb load

user_feature_columns = config['xgb_load']['user_feature_columns']

#linkid2itemcode_dict = load_dict("/home/hadoop/jupyter/sixi/ms_server_1121/resources/linkid2itemcode.pkl")

spu_sku_clean = pd.read_csv(config["xgb_load"]["spu_sku_clean_path"])
spu_sku_clean = spu_sku_clean.drop_duplicates(subset=['item_code'], keep='first')
spu_sku_clean.dropna(subset=['item_code'], axis=0, inplace=True)
spu_sku_clean['sub_category_fix'] = spu_sku_clean['sub_category_fix'].replace({'Breakfast MainCourse': "Breakfast_MainCourse"})

item_code_spu_dict = dict(zip(spu_sku_clean.item_code, spu_sku_clean.sub_category_fix))

xgb_model = load_model(config["xgb_load"]["xgb_model_path"])
daal_model = d4p.get_gbt_model_from_xgboost(xgb_model.get_booster())

item_features = pd.read_csv(config["xgb_load"]["item_features_path"], low_memory=False)
item_feature_dict = {k: list(v) for k,v in item_features.set_index('item_code').T.to_dict(orient = 'series' ).items()}
impute_item_features = [0] * 22

online_feature_name_lst = config['features']['online_features']
user_feature_name_list = config['features']['user_features']
xgb_recom_num = 120